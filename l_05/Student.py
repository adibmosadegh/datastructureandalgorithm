from random import randint
from  pprint import pprint

student_reg = list()
class Student(object):

    def __init__(self, id, name, family, major, year, semester):
        self.id = id
        self.name = name
        self.family = family
        self.major = major
        self.year = year
        self.semester = semester
        student_reg.extend([id, name, family, major, year, semester])

    def add_courses(courses):
        student_reg.extend(courses)


def students_registration():
    id = input("Enter student id: ")
    name = input("Enter student name: ")
    family = input("Enter student family: ")
    major = input("Enter student major: ")
    year = input("Enter year: ")
    semester = input("Enter semester: ")
    Student(id, name, family, major, year, semester)

courseList = list()
def select_unit():

    for i in range(3):
        course = dict()
        course["name"] = input("Enter course name or enter empty to exit: ")
        course["id"] = randint(2000, 4000)
        course["testDate"] = f"1400/{randint(1,12)}/{randint(1,30)}"
        course["classDate"] = f"1400/{randint(1,12)}/{randint(1,30)}"
        courseList.append(course)
    student_reg.extend(courseList)

def delete_courses():
    print(f"Courses: {student_reg[6:]}")
    courseName = input("Enter course Name to Delete: ")
    for i in range(6, len(student_reg)):
        if courseName == student_reg[i]["name"]:
            del student_reg[i]
            break
    print(f"Courses: {student_reg[6:]}")

def add_courses():
    course = dict()
    course["name"] = input("Enter course name to add in your courses: ")
    course["id"] = randint(2000, 4000)
    course["testDate"] = f"1400/{randint(1, 12)}/{randint(1, 30)}"
    course["classDate"] = f"1400/{randint(1, 12)}/{randint(1, 30)}"
    courseList.append(course)
    student_reg.append(course)
    print(student_reg[6:])

def delete_term():
    studentName = input("Enter student name to delete term: ")
    if studentName == student_reg[1]:
        del student_reg[6:]

def delete_student():
    studentName = input("Enter student name to remove from list: ")
    if studentName == student_reg[1]:
        student_reg.clear()


students_registration()
select_unit()
delete_courses()
add_courses()
delete_term()
delete_student()
pprint(student_reg)
print("--------------------Student Information----------------------")
print("  id       name      family       major      year     semester")
print("  ---      ----       -----       -----      ----      -------")
if len(student_reg) > 5:
    print(f"{student_reg[0]}   {student_reg[1]:>4}  {student_reg[2]:>5}  {student_reg[3]:<4}   {student_reg[4]:<2}  {student_reg[5]}")