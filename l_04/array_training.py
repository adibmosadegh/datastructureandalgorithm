from random import randint, choice
from string import ascii_letters
from tabulate import tabulate
from operator import itemgetter
from datetime import datetime


def create_job(num_jobs):
    created_jobs = list()
    jobTime = datetime.now()
    for job_index in range(num_jobs):
        job = dict()
        job["name"] = {
            "Job"
            + choice(ascii_letters)
        }

        job["id"] = randint(0, 10)
        if job["id"] == 0:
            job["level"] = "High Level, System's Important Task"
        else:
            job["level"] = "Low Level"

        job["time"] = f'{jobTime.hour} : {jobTime.minute} : {jobTime.second}'

        created_jobs.append(job)
    print(
        tabulate(sorted(created_jobs, key=itemgetter("id")), headers="keys")
    )
    print("\n ----- Do Jobs Sorted By Time -----")
    for item in sorted(created_jobs, key=itemgetter("time")):
        print(f"{item['name']} with ID : {item['id']} in time : {item['time']} created and in {jobTime} finished.")
    created_jobs[:] = []

    if len(created_jobs) == 0:
        print("------- Was seccessfull --------")
    else:
        print("------- Not Working -------")


create_job(10)
