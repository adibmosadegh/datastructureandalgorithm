This app is written for the fourth chapter of the data structure lesson
Takes numbers from the user and randomly creates tasks with specific names and IDs and saves them in a list
It then removes them from the list in order to perform their tasks

-----------Run the program------------
1: git clone git@gitlab.com:adibmosadegh/datastructureandalgorithm.git
2: cd datastructureandalgorithm/l_04
3: python3 array_training.py

----------Install require modules-----
$pip3 install random string tabulate operator datetime

by Adib Mosadegh
